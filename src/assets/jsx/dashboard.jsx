import Home from "./home";
import Products from "./product";
import Solu from "./solutions";

const dashboardRoutes = [
  {
    path: "/home",
    component: Home
  },
  {
    path: "/products",
    component: Products
  },
  {
    path: "/solutions",
    component: Solu
  },
  { redirect: true, path: "/", to: "/home" }
];

export default dashboardRoutes;
