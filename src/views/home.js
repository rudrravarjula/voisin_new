import React from 'react';
import { Parallax } from 'react-spring';
import '../style.css';
import digiinfo from "../media/MP4/Agile.mp4";
import Container from '../containers/container';



const Page = ({sample, video, offset, caption, first, second,to, gradient, onClick, onClic, defenation1, defenation2,defenation3  }) => (
  <React.Fragment>
    <Parallax.Layer offset={offset} speed={0.2} onClick={onClick}>
      <video id="video-background" loop autoPlay>
        <source src={video} type="video/mp4" />
        <source src={video} type="video/ogg" />
        Your browser does not support the video tag.
      </video>
    </Parallax.Layer>
    <Parallax.Layer offset={offset} speed={0.3} onClick={onClick}>
      <div className="slopeBegin" />
    </Parallax.Layer>
          
    <Parallax.Layer offset={offset} speed={0} onClick={onClick}>
      <div className={`slopeEnd ${gradient}`} />
    </Parallax.Layer>
    
    <Parallax.Layer className="text number" offset={offset} speed={0.3}>
      <span>0{offset + 1}</span>
    </Parallax.Layer>

    <Parallax.Layer className="text header" offset={offset} speed={0.4}>
      <span>
        <p className="cfont">{caption}</p>
        <div className={`stripe ${gradient}`} />
        <p className="sfont">{first}</p>
        <p className="sfont">{second}</p>
        <p className="defenation">{defenation1}</p>
        <p className="defenation">{defenation2}</p>
        <p className="defenation">{defenation3}</p>
      </span>
    </Parallax.Layer>
    <Parallax.Layer className="scup" offset={offset} speed={0} onClick={onClic}>
      <span className="leftarrow"></span>
    </Parallax.Layer>
    <Parallax.Layer className="scdown" offset={offset} speed={0} onClick={onClick}>
      <span className="rightarrow"></span>
    </Parallax.Layer>

  </React.Fragment>
)

export default class Home extends Container {
  scroll = to => this.refs.parallax.scrollTo(to)
  renderChildren() {

    return (
      <React.Fragment>
        
        
                
        <Parallax className="container" ref="parallax" pages={4} scrolling={false}>
          
          <Page offset={0} video={digiinfo} gradient="pink" caption="Digital tranformation" first="Changing the way organisation " second="leverage technologies" onClick={() => this.scroll(1)} 
            defenation1="We build simple human-centric solutions using design & crictical" 
            defenation2="thinking approch to help organisation inovate and grow"
            defenation3= ""
            to="/1"
            onClic={() => this.scroll(0)}
          />
          <Page offset={1} gradient="teal" caption="Complete Telephony solution" first="One stop solution for" second="enterprise telephony" onClick={() => this.scroll(2)} 
          defenation1="The Award winning enterprise telephony solution based on " 
          defenation2="Open source Asterisk - gives your business a voice"
          defenation3= ""
          to="/2"
          onClic={() => this.scroll(0)}
          />
          <Page offset={2} gradient="tomato" caption="Transforming Data Center" first="Cost effective solutuons" second="for data centers" onClick={() => this.scroll(3)} 
          defenation1="Transform your datacenter to simple-yet-robust modern datacenter which is "  
          defenation2="modular, reliable and resilient - resulting faster ROI"
          defenation3= ""
          to="/3"
          onClic={() => this.scroll(1)}
          />
          <Page offset={3} gradient="yellow" caption="AI & Analytic solutions" first="Transform your data" second="into a useful information" onClick={() => this.scroll(0)} 
          defenation1="Don't make your data sit idle. Power your data with our" 
          defenation2="AI & Analytics platforms to exponentially improve & grow your business"
          defenation3= ""
          to="/4"
          onClic={() => this.scroll(2)}
          />
        </Parallax>
            </React.Fragment>
            )
          }
}