import React from 'react';
import Container from '../containers/container';
import '../style.css';

import GridContainer from '../components/Grid/GridContainer';
import GridItem from "../components/Grid/GridItem.jsx";
import { Grid } from 'material-ui';
import { NavLink } from 'react-router-dom';
import AI from "../media/icons/devops.png";
import java from "../media/icons/java.png"
import Course from "../components/Products/course" 
  const courses = [{logo:AI,alt:"AI",title:"Database Developer",link:"/database",color:"blue",title:"titile1",content:"content1"},
  {logo:AI,alt:"AI",title:" Data Warehousing.",link:"/warehousing",color:"blue",title:"titile2",content:"content2"},
  {logo:AI,alt:"AI",title:"Web Designing",link:"/web",color:"blue"},
  {logo:java,alt:"java",title:"Java",link:"/java",color:"blue"},
  {logo:AI,alt:"AI",title:"Software Testing",link:"/ai",color:"blue"},
  {logo:AI,alt:"AI",title:"Mobile Application",link:"/ai",color:"blue"},
  {logo:AI,alt:"AI",title:"Cloud Computing",link:"/ai",color:"blue"}]
  
export default class Home extends Container {
  scroll = to => this.refs.parallax.scrollTo(to)
  
  renderChildren() {

    return (
      <div className="container2">
        <GridContainer>
        { courses.map((course,index) => (
        
                <Course
                    key={index}
                    color={course.color}
                    course_logo={course.logo}
                    course_alt={course.alt}
                    course_title={course.title}
                    course_link={course.link}
                />
            
        ))}
        </GridContainer>   
      </div>
    )
  }
}