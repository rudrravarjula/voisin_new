import React from "react";
import voisin from "../media/voisin.png";
import '../style.css';
import pencil from "../media/Learrning Lab/pencil.png";
import { NavLink } from "react-router-dom";
import right from "../media/right.png"
import GridContainer from '../components/Grid/GridContainer';
import GridItem from "../components/Grid/GridItem.jsx";
import network from "../media/Learrning Lab/Networkers_Bootcamp.png";
import bigdata from "../media/Learrning Lab/BigData_Bootcamp.png";
import developer from "../media/Learrning Lab/Developers_Bootcamp.png";
import security from "../media/Learrning Lab/Security_Bootcamp.png";

const Button = ({button}) => (
    <div className="round-button">
        <NavLink to="" className="b80">{button}</NavLink>
        <img src={right} className="c20" alt="" width="50" height="40"/>
    </div>
  )
  const Strip = ({mainhead,nexthead,ori,texori}) => (
    <div className={`bbstrip ${ori}`}>
        <div className="strblue">
            <span></span>
        </div>
        <div className={`strblack ${texori}`}>
            <h1>{mainhead}</h1>
            <h2>{nexthead}</h2>
        </div>
    </div>
  )
  const Programs =({plogo,ptitle,pcolor}) => (
     <div className="prog">
        <div className="plogo">
            <img src={plogo} alt="" width="120" height="120"/>
        </div>
        <div className={`ptitle ${pcolor}`}>
            <h3>{ptitle}</h3>
        </div>
     </div> 
  )
export default class Products extends React.Component{
    render() {
      return (
        <div className="bootcamp">
            <div className="section1">
                <div className="translayer">
                <div className="sec1_div1">
                <div className="voisin_logo">
                    <img src={voisin} alt=""/>
                    <div>
                        <h1>Are you IT Aspirant</h1>
                        <h1>Looking for Job?</h1>
                        <h1>Are you a student?</h1>
                    </div>
                </div>
                <div className="bootcamp_logo">
                <img src={pencil} alt="" height="365" width="45"/>
                    <h1><span className="learning">Learning</span> <span className="labs">Labs</span></h1>                          
                </div>
                </div>
                <div className="sec1_strip">
                    <p>You are at right place</p>
                </div>
                <div className="sec1_dis">
                    <GridContainer>
                        <GridItem sm={4} md={4} lg={3} className="middle">
                            <center><Button button="Join Now"/></center>
                        </GridItem>
                        <GridItem sm={8} md={8} lg={9} className="right">
                            <p>Self Start your career with VOISIN Bootcamp To boot up your IT skills</p>
                        </GridItem>
                    </GridContainer>
                </div>
                </div>
            </div>
            <div className="section2">
                <div className="sec2con">
                <div className="sec2p">
                    <p>VOISIN Bootcamp is a OTP. The One time program design to provide a personal a personal interest on every candidate. We belive every individual has diffrent level of understanding and course should be coustomize accordingly, We just do that.</p>
                    <p>OTP introduced you to the top technologies making you polyglot in wide varieties od domines. The program Blended with a perfect recipe makes you a career ready.</p>
                </div>
                <center className="sec2bt"><Button button="Talk To Us"/></center> 
                </div>
            </div>
            <div className="strip">
                <Strip 
                    mainhead="One Time Program (OTP)" 
                    nexthead="Technology train by technologists"
                    ori="fblue"
                    texori="blright"
                />
            </div>
            <div className="section3">
                <GridContainer>
                    <GridItem sm={6} md={4} lg={3}>
                        <Programs
                        plogo={network}
                        ptitle="Networks Bootcamp"
                        pcolor="cgray"
                        />
                    </GridItem>
                    <GridItem sm={6} md={4} lg={3}>
                        <Programs
                        plogo={developer}
                        ptitle="Developer Bootcamp"
                        pcolor="clgray"
                        />
                    </GridItem>
                    <GridItem sm={6} md={4} lg={3}>
                        <Programs
                        plogo={security}
                        ptitle="Security Bootcamp"
                        pcolor="cgray"
                        />
                    </GridItem>
                    <GridItem sm={6} md={4} lg={3}>
                        <Programs
                        plogo={bigdata}
                        ptitle="Bigdata & AI Bootcamp"
                        pcolor="clgray"
                        />
                    </GridItem>
                </GridContainer>
            </div>
            <div className="strip">
            <Strip 
                    mainhead="Contact us @ 9962560024" 
                    nexthead="learning@voisintechnologies.com"
                    ori="fblack"
                    texori="blcent"
                />
            </div>
            <div className="blueimg">
                <div className="joinus"><h1>WHY JOIN US</h1></div>
                <div className="blueback"><p>Unique structured pattern of training without curriculum. Brings you a new way of learning experince and makes you a corporate work culture</p></div>
            </div>
        </div>
    )
    }
}

