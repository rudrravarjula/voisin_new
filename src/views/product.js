import React from 'react';
//import { Parallax } from 'react-spring'
import GridContainer from "../components/Grid/GridContainer.jsx";
import GridItem from "../components/Grid/GridItem.jsx";
import Head from "./header";
const Product = ({ title, icon, prodis, prolink}) => (
    <React.Fragment>
      <div className="produc">
        <div className="proname"><span>{title}</span></div>
        <div className="proicon">{icon}</div>
        <div className="prodis"><p>{prodis}</p></div>
        <div className="prolink">
            <a className="prolin" href="/readmore">Read more</a>
        </div>
      </div>
    </React.Fragment>
  )

export default class Products extends React.Component{
    render() {
      return (
      <div className="container2">
            <Head />    
            <GridContainer className="gridpro">
              <GridItem xs={12} sm={6} md={4} lg={3}>
            <center><Product
                title="TELECOM"
                icon=""
                prodis="A complete telephony solution
                suits all type of enterprises, from
                small to large. Now double your
                communication at half cost."

            /></center>
            </GridItem>
            <GridItem xs={12} sm={6} md={4} lg={3}>
            <center><Product
                title="REFURBISHED SERVERS"
                prodis="A complete telephony solution
                suits all type of enterprises, from
                small to large. Now double your
                communication at half cost."

            /></center>
            </GridItem>

            <GridItem xs={12} sm={6} md={4} lg={3}>
            <center><Product
                title="IP BVX"
                prodis="A complete telephony solution
                suits all type of enterprises, from
                small to large. Now double your
                communication at half cost."

            /></center>
            </GridItem>
            <GridItem xs={12} sm={6} md={4} lg={3}>
            <center><Product
                title="IP BVX"
                prodis="A complete telephony solution
                suits all type of enterprises, from
                small to large. Now double your
                communication at half cost."

            /></center>
            </GridItem><GridItem xs={12} sm={6} md={4} lg={3}>
            <center><Product
                title="IP BVX"
                prodis="A complete telephony solution
                suits all type of enterprises, from
                small to large. Now double your
                communication at half cost."

            /></center>
            </GridItem>

            </GridContainer>


      </div>
      )
    }
}

