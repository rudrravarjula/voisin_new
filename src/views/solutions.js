import React from 'react';
import { Parallax } from 'react-spring';
import '../style.css';
import Container from '../containers/container';
import astes from "../media/astecs.png"
import datacenter from "../media/datacenter.png"
import datavideo from "../media/asttecs2.mp4"

const Page = ({sample, image, offset, disp,disp2,  first, second, spos, sori, onClick, onClic,video }) => (
  <React.Fragment>
    <Parallax.Layer offset={offset} speed={0.3} onClick={onClick}>
    <div className="slopeBegin" />
    </Parallax.Layer>
    <Parallax.Layer  className={`simage ${spos}`} offset={offset} speed={0} onClick={onClick}>
    {video !== undefined ? (
                  <video width="500" height="500" className="ima" autoPlay="true" loop="true">
                   <source src={video} type="video/mp4" />
                  </video>
                  ) : <img src={image}  className="ima" alt="solution" height="500" width="450"/>
                }
        
      
    </Parallax.Layer>

    <Parallax.Layer className={`stitle ${sori}`} offset={offset} speed={1} onClick={onClick}>
      
        <span className="first">{first}</span>
        <span className="second">{second}</span>
      
    </Parallax.Layer>

    <Parallax.Layer className={`sdisp ${sori}`} offset={offset} speed={10} onClick={onClick}>
      <p>{disp}</p>
      <p>{disp2}</p>
    </Parallax.Layer>
    <Parallax.Layer className="scup" offset={offset} speed={0} onClick={onClic}>
      <span className="leftarrow black"></span>
    </Parallax.Layer>
    <Parallax.Layer className="scdown" offset={offset} speed={0} onClick={onClick}>
      <span className="rightarrow black"></span>
    </Parallax.Layer>

      
  </React.Fragment>
)

export default class Home extends Container {
  scroll = to => this.refs.parallax.scrollTo(to);

  renderChildren() {

    return (
      <div>
        
        <Parallax className="container4" ref="parallax" pages={3} scrolling={false}>
          <Page offset={0}
          spos="spoleft"
          sori="sorileft"
          first="Telecom" 
          video={datavideo}
          image={astes}
          second="Solutions" 
          disp="For Voisin, telecom is a mature yet continuously evolving
          practice. Asterisk is a complete telephone solutions platform. *astTECS offers open source asterisk solutions in India, addressing a wide range of call center / contact center solutions & enterprises solutions including – IVR systems & solutions, open source VoIP PBX, call center dialer, voice dialer, voice logger & voice recorder, automatic call distribution or ACD application software, predictive dialer, hosted dialer, GSM card for asterisk, VoIP call recording, customized line call recording / monitoring / tracking.*astTECS is today geared to provide cost effective telephony solutions. Our asterisk solutions are feature rich, extremely versatile, flexible, customizable & scalable. For more details on Asterisk Systems & Solutions Contact us Today!"
          onClick={() => this.scroll(1)}
          onClic={() => this.scroll(0)}
          />
          <Page offset={1}
          spos="sposright"
          sori="soriright"
          first="Data center" 
          
          image={datacenter}
          second="Solutions" 
          disp="Technology form the back bone of every enterprise, and without it, surviving the dynamic business landscape of 21st century becomes an impossibility. Our solutions aren't built to sustain competition, but to help you thrive amidst it. We have the prowess to design and implement customized, scalable data center solutions for your enterprise. Voisin aims to bring the revolution in the IT infrastructure Management, with the cutting edge solutions to our esteemed client. As we understood the current IT needs and challenges, we build the tailored solutions for every individual customer. Our solutions are futuristics, flexible and scalable. We pioneer in efficient energy, cooling, optimization, monitoring and moving the datacenter. Talk to us to know more about various data center solutions and amplify your business growth."
          onClick={() => this.scroll(2)}
          onClic={() => this.scroll(0)}
          />
          <Page offset={2}
          spos="spoleft"
          sori="sorileft"
          first="Artificial Intelligence" 
          image="https://www.accruent.com/sites/default/files/styles/resource_image/public/resource-images/blog-images/telecom%20tower%201000x550.png?itok=FaLR8sOm"
          second="& Machine Learning" 
          disp="Organisations are generating huge bytes of data. Data will become a dead data after certain period of time. These data can be made meaningful to help predict future and make decisions appropriately. Data can be analysed manually when the amount of data is less. Humans are tend to errors. And with Terra/Zeta bytes of data, its time consuming, error prone and value of such result is worth only when it was started. These problems can be overcome by training the machines to do the analysis. The result can be immediate. Real-time analysis will help organisation gain insights with updated data. 
          "
          disp2={(<React.Fragment>We are building AI platforms which will transform the industries the way they look at data. We are specialised in <strong>HealthCare:</strong> Diagnosis in Medical Imaging, <strong>Banking:</strong> Demand Forecasting, Detect Fraud, <strong>Retail:</strong> Optimised Pricing, Product Positioning, helping the customers increase in lead conversion, increase in revenue, efficient operations etc.</React.Fragment>)}
          onClick={() => this.scroll(0)}
          onClic={() => this.scroll(1)}
          />
          
         </Parallax>
            </div>
            )
          }
}