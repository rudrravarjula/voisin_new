import React from 'react';
import List from "material-ui/List";
import withStyles from "material-ui/styles/withStyles";
import ListItem from "material-ui/List/ListItem";
import Header from "../components/Header/Header.jsx";
import navbarsStyle from "../assets/jsx/navbarsStyle.jsx";
import voisin from "../media/voisin.png";
import { NavLink } from 'react-router-dom'
import "../style.css";
class Head extends React.Component{
  constructor(props){
    super(props);
    console.log(props);
    this.state = {
        current: props.match.url  
    };
}

toggle(path) {
    this.setState({
        current: path
    });
}

    render() {
      const current = this.state.current;
      console.log(this.props, '*****', current);
      const { classes } = this.props;
      return (
        
            <Header
              brand={voisin}
              color="white"
              fixed={true}
              rightLinks={
                <List className={classes.list}>
                  <ListItem className={`${classes.listItem} ${current==='/'?'activ':''}`} onClick={() => this.toggle('/')} >
                    <NavLink to="/" className="navbuton">
                      Home
                    </NavLink>
                  </ListItem>
                  <ListItem className={`${classes.listItem} ${current==='/solutions'?'activ':''}`} onClick={() => this.toggle('/solutions')}>
                    <NavLink to="/solutions" className="navbuton">
                      Solutions
                    </NavLink>
                  </ListItem>
                  <ListItem className={`${classes.listItem} ${current==='/products'?'activ':''}`} onClick={() => this.toggle('/products')}>
                  <NavLink to="/products" className="navbuton"> 
                      Products
                    </NavLink>
                  </ListItem>
                  <ListItem className={`${classes.listItem} ${current==='/bootcamp'?'activ':''}`} onClick={() => this.toggle('/bootcamp')}>
                  <NavLink target="_blank" to="/bootcamp" className="navbuton"> 
                      Bootcamp
                    </NavLink>
                  </ListItem>
                  <ListItem className={`${classes.listItem} ${current==='/about'?'activ':''}`} onClick={() => this.toggle('/about')}>
                  <NavLink to="/about" className="navbuton">
                      About Us
                    </NavLink>
                  </ListItem>
                </List>
              }
            />
        
              
    )
    }
}
export default withStyles(navbarsStyle)(Head);