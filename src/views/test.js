import React from "react";
import PropTypes from "prop-types";
import { Switch, Route, Redirect } from "react-router-dom";
// creates a beautiful scrollbar

import List from "material-ui/List";
import withStyles from "material-ui/styles/withStyles";
import ListItem from "material-ui/List/ListItem";
import Button from "./CustomButtons/Button.jsx";
import Header from "./Header/Header.jsx";
import navbarsStyle from "./Header/navbarsStyle.jsx";
import voisin from "./voisin.png";
import appStyle from "./appstyle.jsx";
import dashboardRoutes from "./dashboard.jsx";



const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

<div className="container2">
            
<Head />    
<GridContainer className="gridpro">
  <GridItem xs={12} sm={6} md={4} lg={3}>
<center><Product
    title="TELECOM"
    icon=""
    prodis="A complete telephony solution
    suits all type of enterprises, from
    small to large. Now double your
    communication at half cost."

/></center>
</GridItem>
<GridItem xs={12} sm={6} md={4} lg={3}>
<center><Product
    title="CRM"
    prodis="A complete telephony solution
    suits all type of enterprises, from
    small to large. Now double your
    communication at half cost."

/></center>
</GridItem>

<GridItem xs={12} sm={6} md={4} lg={3}>
<center><Product
    title="DATA CENTER"
    prodis="A complete telephony solution
    suits all type of enterprises, from
    small to large. Now double your
    communication at half cost."

/></center>
</GridItem>
<GridItem xs={12} sm={6} md={4} lg={3}>
<center><Product
    title="REFURBISHED SERVERS"
    prodis="A complete telephony solution
    suits all type of enterprises, from
    small to large. Now double your
    communication at half cost."

/></center>
</GridItem><GridItem xs={12} sm={6} md={4} lg={3}>
<center><Product
    title="SECURITY SYSTEMS"
    prodis="A complete telephony solution
    suits all type of enterprises, from
    small to large. Now double your
    communication at half cost."

/></center>
</GridItem>


</GridContainer>


</div>
class App extends React.Component {
  getRoute() {
   
  }
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className={classes.wrapper}>
        <Header
              brand={voisin}
              color="white"
              fixed="True"
              rightLinks={
                <List className={classes.list}>
                  <ListItem className={classes.listItem}>
                    <Button
                      href={dashboardRoutes}
                      className={classes.navLink}
                      onClick={e => e.preventDefault()}
                      color="transparent"
                      
                    >
                      Solutions
                    </Button>
                  </ListItem>
                  <ListItem className={classes.listItem}>
                    <Button
                      href={dashboardRoutes}
                      
                      className={classes.navLink}
                      onClick={e => e.preventDefault()}
                      color="transparent"
                    >
                      Products
                    </Button>
                  </ListItem>
                  <ListItem className={classes.listItem}>
                    <Button
                      href={dashboardRoutes}
                      className={classes.navLink}
                      onClick={e => e.preventDefault()}
                      color="transparent"
                    >
                      AboutUs
                    </Button>
                  </ListItem>
                </List>
              }
            />
        <div className={classes.mainPanel} ref="mainPanel">
          {this.getRoute() ? (
            <div className={classes.content}>
              <div className={classes.container}>{switchRoutes}</div>
            </div>
          ) : (
            <div className={classes.map}>{switchRoutes}</div>
          )}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(appStyle)(App);
