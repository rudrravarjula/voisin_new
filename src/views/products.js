import React from "react";
import { Route } from "react-router-dom";
//import { Parallax } from 'react-spring'
import Sidenav from "./sidenav";
import Product from "../components/Products/Prorduct_layout";
import Container from "../containers/container";
import features from "../media/features (2).png"
import crm from "../media/CRM.png";
import ippbx from "../media/ippbx4.png";
import call from "../media/callcenter.png"
import ipphone from "../media/ipphone.png"
import ivr from "../media/IVR.png"
import prigate from "../media/PPRI Gateway.png"
import pri from "../media/pri.png"
import smartphone from "../media/SPGW.png"
import video from "../media/VC.png"
import voice from "../media/voice logger.png"
import gsm from "../media/GSM gw.png"
import customer from "../media/customer experiance.png"
import organised from "../media/organized data.png"
import efficiency from "../media/effeciancy.png"
import relationship from "../media/relationship management.png"
import sales from "../media/sales.png"

const Crm_pro =[{icon:relationship,title:"Relationship Management",description:"With complete awareness about the customer, customer’s needs & past interaction with company, an employee can create a better bond with the customer, and help him better in less time."},
{icon:efficiency,title:"Efficiency",description:"Information on customer buying behavior & interest can be used to make faster decisions & close more sales"},
{icon:customer,title:"Customer Experience",description:"Customer will have less struggle everytime he approaches to the organization. All the department within an organization will have complete data about the customer."},
{icon:organised,title:"Organized Data",description:"All your leads, contacts, customers & orders can be organized & stored in an standard format, which is easy and flexible to retrieve."},
{icon:sales,title:"Enhanced Sales & Marketing",description:"Cross selling, Up selling, Calls, Newsletters & Emails can be more precise and result oriented when it is delivered to a focused group."}];

const Data_pro =[{icon:relationship,title:"Datacenter Consultant",description:"Voisin provides end-to-end consultancy for your existing datacenter. We poineered in energy, Capacity, Cooling and infrastructure management of data center. Our complete analysis helps you to reduce overall downtime, operational costs and increase efficiency of your datacenter.",li1:"Infrastructure managment",li2:"Network managment",li3:"Data center Optimisation",li4:"Solar data center",li5:"Brown and Green feild support",li6:"Data center monitering ",},
{icon:efficiency,title:"Console (KVM) Management",description:"The holistic and cost effective NextGEN console management solution from voisin enables your IT infrastructure assests accessible from any part of the world.",li1:"Secure Remote access",li2:"Auto discovery",li3:"Easy Integration",li4:"Console managment",li5:"Power PDU managment",li6:"Centralized remote access",},
{icon:customer,title:"nXTGEN Colling",description:"Voisin Cooling solutions are flexible and effictive over the traditional collings systems. nXTGEN colling are reaiable,ROI-focused and world class infrastructure makes your datacenter optimised and reduce costs.",li1:"RDHx",li2:"Coling system",li3:"Data center in a box",li4:"Lower TCO - rapid ROI",li5:"Reducing Opex cost",li6:"Efficient heat transfer",},
{icon:customer,title:"Datacenter Relocation",description:"Voisin team with a ample experiance in datacenter movement either on cloud or physicle movement we just make it with less down downtime and more effictive way",li1:"Minimal down time",li2:"Colud/P2V migrations",li3:"AGILE methodology",li4:"Sleamless migratation",li5:"Zero touch IP change",li6:"Post migration support",}];

const Server_pro =[{icon:relationship,title:"Servers",description:"A global distributor of used or refurbised servers. Our Inventory includes IBM, DELL, HP, etc. We also provide consultancy to choose best fit severs for your need.",},
{icon:efficiency,title:"Storage",description:"Choosing a right storage is always a challange. We help you select a right product from Tapes to Drives and Disk Trays or be a SAN storage."},
{icon:customer,title:"Networking Devices",description:"All used networking devises Switchs Routers and Firewall from the leading manufactures includes CISCO, JUNIPER, NORTAL, D-LINK, etc under one roof"},
{icon:customer,title:"Laptops & Desktops",description:"Be it a home or for comercial use Laptops or Desktops is become need of an hour. we offers New, Used, Refurbished Devices with competative price"}];

const Et_products =[{icon:ippbx,title:"IP PBX",description:"A complete telephony solution suits all type of enterprises, from small to large. Now double your communication at half cost.",li1:"Web based reception console",li2:"Smart phone with WIFI client",li3:"Video calling",li4:"Integratiojn with EPABX",li5:"Distributed office setup",li6:"Voice logger",},
{icon:call,title:"Call Center Dialer",description:"Handle large volumes of calls smartly by using astTECS Dialers. Available for both in bound & outbound processes.",li1:"PRI Integration",li2:"Voice logger",li3:"GSM Integration",li4:"A2 Billing",li5:"Report Management",li6:"Lead Management",},
{icon:smartphone,title:"SmartPhone Gateway",description:"Prioritize your emails by sales pipeline, with context and analytics.",li1:"Call record management",li2:"Landline calls in smartphone",li3:"Echo cancilation",li4:"Mobile call center",li5:"Boundry less connection",li6:"Integration with EPABX",},
{icon:video,title:"Video Conference",description:"Connect & collaborate with your clients, partners or employees using view calls. Reduce the travel expenses & increase the productivity of every interaction.",li1:"Video conference",li2:"E-learning conference",li3:"Cloud based recordings",li4:"Hospital room conference",li5:"Hottel room conference",li6:"Rent or Buy in demand",},
{icon:ivr,title:"IVR System",description:"Let technology filter your calls traffic. A smart solution to handle large call volumes with limited resources, personalized messages & call routing.",li1:"Real time monetring",li2:"Incresed productivity",li3:"preortised coustomer calls",li4:"Voice blasting",li5:"Automate out bound call",li6:"Easy Backend integration",},
{icon:voice,title:"Voice Logger",description:"Use call logging or agent monitoring to improve your performance with your customers. Get regular reports, listen to recorded files, or review agent’s performance.cost.",li1:"Records all calls",li2:"Search capabulity",li3:"Data security",li4:"Uniified interface",li5:"Data security",li6:"Easy reporting",},
{icon:gsm,title:"GSM Gateway",description:"Reduce the company’s telephone expenses by reduced the call rates. Also, send messages via “SMS”.t.",li1:"Mobile to Mobile calls",li2:"Least cost Routing",li3:"DTMF Detection",li4:"Balance alarm",li5:"Sending or receiving sms",li6:"White & Black list",},
{icon:ipphone,title:"IP Phones",description:"Prioritize your emails by sales pipeline, with context and analytics.",li1:"Call transfer(Blined,Busy,Asked)",li2:"Call hold/Waitng",li3:"Call Return/Forward",li4:"D and D",li5:"Hands Free indicator",li6:"Supports Pc control",},
{icon:pri,title:"PRI Cards",description:"Prioritize your emails by sales pipeline, with context and analytics.",li1:"Compatable with multi platform",li2:"Hardware Echo cancilation",li3:"Avilable in PCIe",li4:"PRI Switch compatabulity",li5:"Superier voice quality",li6:"Complex integrations",},
{icon:prigate,title:"PRI Gateway",description:"Prioritize your emails by sales pipeline, with context and analytics.",li1:"Call routing",li2:"Call manuplation",li3:"Integrated SIP registrar",li4:"Advice of charge(AoC)",li5:"Full IP routing",li6:"Remote Provesining",}];
const Enterprise = () => (
    <React.Fragment>
        <Product 
            title="*astTECS a complete Telephony solutions for Enterprise."
            discription="During any conversation, one with better information dominates. When a customer calls, respond with 100% efficiency."
            background="astec_color"
            background1="astec_color1"
            products ={Et_products}
            >
            <div className="contact_info">
                <span>Talk to us and lets together make diference....</span><br />
                <span className="palegreen">Email-contact@voisintechnologies.com</span><span className="palegreen"> Mobile:9962560024</span>
            </div>
            <div className="product_about">
            <h2>About *astTECS</h2>
            
            <p>Asterisk is a complete telephone solutions platform. *astTECS offers open source asterisk solutions in India, addressing a wide range of call center / contact center solutions & enterprises solutions including – IVR systems & solutions, open source VoIP PBX, call center dialer, voice dialer, voice logger & voice recorder, automatic call distribution or ACD application software, predictive dialer, hosted dialer, GSM card for asterisk, VoIP call recording, customized line call recording / monitoring / tracking.For more details on Asterisk Systems & Solutions Contact us Today!</p>
            </div>
            <div className="product_features">
                <span>Open source Asterisk features</span>
                <img src={features} alt="features" />
            </div>
        </Product> 
    </React.Fragment>
);
const Crm =() => (
    <React.Fragment>
        <Product 
            title="*astTECS Call center dailer with integrated CRM."
            discription="During any conversation, one with better information dominates. When a customer calls, respond with 100% efficiency."
            products={Crm_pro}
            background="crm_color"
            background1="crm_color1"
            >
            <div className="contact_info">
                <span>Talk to us and lets together make diference....</span><br />
                <span className="palegreen">Email-contact@voisintechnologies.com</span><span className="palegreen"> Mobile:9962560024</span>
            </div>
            <div className="product_features">
                <span>CRM features</span> 
                <img src={crm} alt="CRM" className="crmimg"/>
            </div>
            
        </Product>
    </React.Fragment>
);
const Datacenter =() => (
    <React.Fragment>
        <Product 
            title="Revolutionary Technologies for the data center under one umbrella. "
            discription="During any conversation, one with better information dominates. When a customer calls, respond with 100% efficiency."
            products={Data_pro}
            background="data_color"
            background1="data_color1"
            >
            <div className="contact_info">
                <span>Talk to us and lets together make diference....</span><br />
                <span className="palegreen">Email-contact@voisintechnologies.com</span><span className="palegreen"> Mobile:9962560024</span>
            </div>
        </Product>
    </React.Fragment>
);
const Servers =() => (
    <React.Fragment>
        <Product 
            title="One stop shop for IT infrastructure Hardware and Networking"
            discription="During any conversation, one with better information dominates. When a customer calls, respond with 100% efficiency."
            products={Server_pro}
            background="server_color"
            background1="server_color1"
            >
            <div className="contact_info border_space">
                <span>Talk to us and lets together make diference....</span><br />
                <span className="palegreen">Email-contact@voisintechnologies.com</span><span className="palegreen"> Mobile:9962560024</span>
            </div>
        </Product>
    </React.Fragment>
);

export default class Products extends Container{
    renderChildren () {
      return (
        <div className="container2">
        
        <Sidenav match={this.props.match}/>
        <Route exact path={`${this.props.match.url}`} component={Enterprise} />
        <Route path={`${this.props.match.url}/crm`} component={Crm} />
        <Route path={`${this.props.match.url}/datacenter`} component={Datacenter} />
        <Route path={`${this.props.match.url}/servers`}  component={Servers} />
        
        </div>
    )
    }
}

