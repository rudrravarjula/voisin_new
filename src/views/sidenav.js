import React from "react";
import { NavLink } from 'react-router-dom';
import "../style.css";

 
 class Sidenav extends React.Component{
   
    constructor(props){
        super(props);
        this.state = {
            current: '/test'
        };
    }

    toggle(path) {
        this.setState({
            current: path
        });
    }

    
    render( ) {
        
        const current = this.state.current;
        console.log(this.props.match.url);
      return ( 
        <div>

            <nav className="sticky-left-nav top-menu">
                <ul className="stick-left-nav-ul">
                    <li className={`nav-extend ${current==='/test'?'active':''}`} onClick={() => this.toggle('/test')}>
                        <NavLink to={`${this.props.match.url}`} className="a">
                            <span>Enterprise Telephony</span>
                        </NavLink>
                    </li>
                    <li className={`nav-extend ${current==='/test1'?'active':''}`} onClick={() => this.toggle('/test1')}>
                        <NavLink to={`${this.props.match.url}/crm`} className="a">
                            <span>CRM</span>
                        </NavLink>
                    </li>
                    <li className={`nav-extend ${current==='/test2'?'active':''}`} onClick={() => this.toggle('/test2')}>
                        <NavLink to={`${this.props.match.url}/datacenter`} className="a">
                            <span>Data Center</span>
                        </NavLink>
                    </li>
                    <li className={`nav-extend ${current==='/test3'?'active':''}`} onClick={() => this.toggle('/test3')}>
                        <NavLink to={`${this.props.match.url}/servers`} className="a">
                            <span>Servers & Desktop</span>
                        </NavLink>
                    </li>
                    
                </ul>
            </nav>
        </div>
        
        
    )
    }
}
export default Sidenav;