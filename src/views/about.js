import React from "react";

import Container from "../containers/container";
import voisinvalues from "../media/voisin values.jpg";
import Contact from "../components/Contact/contact";

export default class Products extends Container{
    renderChildren () {
      return (
          <React.Fragment>
        <div className="about">
            <div className="about_img">
                <div className="content">
                <h1>About Us</h1>
                <p>Voisin Technologies is a premier IT services and business solutions organization comprised of a      massive team of domain experts from various niches. Multifaceted and diverse, Voisin aims to break-free from the restraints of technological limitations imposed on modern-day businesses, and deliver cutting-edge solutions to solve even the most-challenging of challenges. From enterprising IT solutions that enable SMEs and MSMEs in establishing a strong brand identity to rendering training programs aimed to upskill the workforce of tomorrow, Voisin provides a wide range of services are directed at creating a revolution across multiple verticals.
                </p>
                </div>
                <div className="image">
                    <center><img src={voisinvalues} alt="Voisin Values"/></center>
                </div>
            </div>
            <div className="about_strip">
                <h1>How we are different ?</h1>
                <q>We cannot solve our problems with the same thinking we used when we created them</q>
                <span>--Albert Einstein</span>
                <p>From the heart, For the smart</p>
                <p>
                    Lots of tools and technologies in current competitive world, makes the organization difficult to choose the right solution. Often leading to huge cost to company.  We are competent enough to help you choose the right solution for your needs, reducing CapEx and OpEx cost. With our team from various niches, different skills and with a same mindset, we solve the customer problems uniquely. We treat every customer as a neighbour. We apply the design thinking and criticle thinking to solve the clients complex challenges, making things very simple and adaptable in future.
                </p>
            </div>
            <div className="contact">
                <h1>Contact Us</h1>
                <div className="form">
                <Contact />
                </div>
            </div>
            <div className="contact_info">
                <span>Talk to us and lets together make diference....</span><br />
                <span className="palegreen">Email-contact@voisintechnologies.com</span><span className="palegreen"> Mobile:9962560024</span>
            </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d242.9524245893702!2d80.18181480710355!3d13.020459799426503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0:0xaf02a6cc0e1209ef!2sVoisin+Technologies!5e0!3m2!1sen!2sus!4v1529418468028" width="100%" height="60%" frameborder="0" className="" allowfullscreen></iframe>
        </React.Fragment>
    )
    }
}

