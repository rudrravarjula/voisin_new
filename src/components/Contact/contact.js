import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    style: {color: '#008ba5'},
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 380,
    
  },
  fullWidth:{
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 780,
  },
  menu: {
    width: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
  input: {
    display: 'none',
  },
});

const currencies = [
  {
    value: 'Telecom',
    label: 'Telecom',
  },
  {
    value: 'Data center',
    label: 'Data center',
  },
  {
    value: 'Ai',
    label: 'Artificial Intelligence',
  },
  {
    value: 'Servers',
    label: 'Servers & Storage',
  },
];

class TextFields extends React.Component {
  state = {
    currency: 'Telecom',
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes } = this.props;

    return (
      <form className={`${classes.container}`} noValidate autoComplete="off">
        <TextField
          
          required
          id="fullname"
          label="Full name"
          className={classes.fullWidth}
          margin="none"
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
        />
        <TextField
          required
          id="emailid"
          label="Email ID"
          margin="normal"
          className={classes.textField}
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
        />
        <TextField
          required
          id="mobile"
          label="Phone"
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
          margin="normal"
          className={classes.textField}
        />
        <TextField
          id="company"
          label="Organisation"
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
          margin="normal"
          className={classes.textField}
        />
        <TextField
          id="service/tech"
          select
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
          label="Service/Technology"
          className={classes.textField}
          value={this.state.currency}
          onChange={this.handleChange('currency')}
          SelectProps={{
            MenuProps: {
              className: classes.menu,
            },
          }}
          helperText="Please select your Service/Technology"
          margin="normal"
        >
          {currencies.map(option => (
            <MenuItem key={option.value} value={option.value}>
              {option.label}
            </MenuItem>
          ))}
        </TextField>
        <TextField
          id="textarea"
          InputLabelProps={{ style: { fontFamily: 'Arial', color: '#008ba5'}}}
          label="Let Us Know how we can assest you"
          multiline
          className={classes.fullWidth}
          margin="normal"
        />
        <h2 className="contact_agree">Yes, I would  like voisin Technologyies to contact me and keep inform about products and services </h2>
        <Button variant="contained" color="primary" className={classes.button}>
        I Agree -> Submit
      </Button>
      </form>
    );
  }
}

TextFields.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFields);