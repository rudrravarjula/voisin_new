import React from 'react';
import GridContainer from '../Grid/GridContainer';
import GridItem from "../Grid/GridItem.jsx";
import Items from "./Items_Layout";

const Product = ({ title, discription,children, products,background,background1}) => (
<React.Fragment>
    <div className="product">
        <div className={`astecs ${background}`}>
        <h2 className="product_title">{title}</h2>
        <p className="product_discription">{discription}</p>
        </div>
        <center><hr className={`hrr ${background1}`} /></center>
        <div className="item">
      <GridContainer alignItems="center"> 
      { products.map((product,index) => (
        <GridItem sm={6} md={3} lg={3} key={index} >
        <Items
            item_icon={product.icon}
            item_title={product.title}
            item_discription={product.description}
            background={background1}
            li1={product.li1}
            li2={product.li2}
            li3={product.li3}
            li4={product.li4}
            li5={product.li5}
            li6={product.li6}
            li7={product.li7}
        />
        </GridItem>
      ))}
      </GridContainer>
      </div>
        {children}
    </div>
    
</React.Fragment>
)
export default Product
