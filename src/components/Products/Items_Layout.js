import React from "react";

const Items =({item_icon,item_title,item_discription,li1,li2,li3,li4,li5,li6,li7,li8,background}) =>(
    <React.Fragment>
        <div className="card">
        <div className="items">
            <span className="item_icon"><img src={item_icon} alt="icon"/></span>
            <span className="item_title">{item_title}</span>
            <p className="item_discription">{item_discription}</p>
        </div>
        <div className={`back ${background}`}>
            <ul className="backul">
                <li>{li1}</li>
                <li>{li2}</li>
                <li>{li3}</li>
                <li>{li4}</li>
                <li>{li5}</li>
                <li>{li6}</li>
                <li>{li7}</li>
                <li>{li8}</li>
            </ul>
        </div>
        </div>
    </React.Fragment>
);
export default Items