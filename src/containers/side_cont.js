import React from "react";
import Sidenav from "../views/sidenav";
export default class Side_cont extends Container{

    renderChildren() {
        return (
            <React.Fragment>
               <Sidenav match={this.props.match}/>
                {this.renderSideChildren()}
            </React.Fragment>
        );
    }
}