import React from "react";
import Head from "../views/header";
export default class Container extends React.Component{

    render() {
        return (
            <React.Fragment>
                <Head match={this.props.match}/>
                {this.renderChildren()}
            </React.Fragment>
        );
    }
}