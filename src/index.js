import React from "react";
import ReactDOM from "react-dom";
import "./style.css"
import Home from "./views/home";
import Solutions from "./views/solutions";
import Products from "./views/products";
import { createBrowserHistory } from "history";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import About from "./views/about";
import bootcamp from "./views/bootcamp"

//import Head from "./views/header"
var hist = createBrowserHistory();

ReactDOM.render(
<BrowserRouter history={hist}>  
    <Switch>
      <React.Fragment>
        <Route exact path='/' component={Home} />           
        <Route path='/solutions' component={Solutions} />           
        <Route path='/products' component={Products} />
        <Route path='/bootcamp' component={bootcamp} />
        <Route path='/about' component={About}/>                 
      </React.Fragment> 
    </Switch>
  </BrowserRouter>,
  document.getElementById("root")
);

